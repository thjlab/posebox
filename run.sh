#!/bin/bash

CLIENT=0

POSITIONAL=()
while [[ $# -gt 0 ]]
do

  key="$1"

  case $key in
    -m|--mode)
      MODE="$2"
      shift
      shift
      ;;
    *)
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

set -- "${POSITIONAL[@]}"

if [[ "${MODE}" = "server" ]]; then

  PBOX_FOLDER=/opt/posebox

  python3 ${PBOX_FOLDER}/main.py -c ${PBOX_FOLDER}/config.yaml

else

  PBOX_CONFIG=$(pwd)/config.yaml

  python3 main_client.py -c ${PBOX_CONFIG}

fi
