#pylint: disable=no-member
"""
Module supporting the Camera class, responsible for taking snapshots.

class Camera: contains the builder and the main capture function.

"""

import os
import logging

import cv2

class Camera:
    """Class responsible for taking frames captures from webcam.

    This class initiates a Camera object and then invoke the main method
    called capture. Directly capture frames from the existing webcam.
    Relies on cv2 from frames capture and save.

    Attributes:
        device = An integer indicating the webcam device to use.
        path = A string indicating the path where frames are saved.
    """

    def __init__(self, path='./', device=0, output='output.mkv', frame_nbr=500):
        """Init Camera with device nbr and path."""

        self._path = path
        self._device = device
        self._output_name = output
        self._frame_nbr = frame_nbr


    @property
    def path(self):
        """Getter for Camera instance path.

        Args:
            None

        Returns:
            A string representing the path where frames are saved.
        """

        return self._path


    @path.setter
    def path(self, path):
        """Setter for Camera instance path.

        Args:
            path: A string representing the new path.

        Returns:
            None
        """

        self._path = path


    @property
    def device(self):
        """Getter for Camera instance device.

        Args:
            None

        Returns:
            An int representing the webcam device.
        """

        return self._device


    @device.setter
    def device(self, device):
        """Setter for Camera instance device.

        Args:
            path: An int representing the new device.

        Returns:
            None
        """

        self._device = device


    @property
    def output_name(self):
        """Getter for Camera instance output_name.

        Args:
            None

        Returns:
            A string representing the name of the video output.
        """

        return self._output_name


    @output_name.setter
    def output_name(self, output):
        """Setter for Camera instance output_name.

        Args:
            output: A string representing the new output name.

        Returns:
            None
        """

        self._output_name = output


    @property
    def frame_nbr(self):
        """Getter for Camera instance frame_nbr.

        Args:
            None

        Returns:
            An int representing the number of frames to save.
        """

        return self._frame_nbr


    @frame_nbr.setter
    def frame_nbr(self, frame_nbr):
        """Setter for Camera instance frame_nbr.

        Args:
            frame_nbr: An int representing the number of frames to save.

        Returns:
            None
        """

        self._frame_nbr = frame_nbr


    def capture(self):
        """Capture frames from webcam.

        Starts the existing camera bound to the computer and starts
        taking snapshots, or frames. Relies on the cv2 library call
        for snapshots. Takes snapshots and saves them under self.path.

        Args:
            None

        Returns:
            None
        """

        cap = cv2.VideoCapture(self.device)
        _, frm = cap.read()

        cv2.imwrite(os.path.join(self.path, "frame.jpg"), frm)
        cap.release()


    def write_video(self, fps=10):
        """Write a video based on camera capture.

        Captures a certain number of frames and write a video out of it,
        encoded in h264 format, which supports high compression features.
        The CV2 implementation needs to support H264 compression capabilities,
        which is not always the case by default.

        Args:
            fps: An int representing the output video frames per second.

        Returns:
            None
        """

        cap = cv2.VideoCapture(self.device)
        output_name = os.path.join(self.path, self.output_name)

        fourcc = cv2.VideoWriter_fourcc(*'H264')
        out = cv2.VideoWriter(output_name, fourcc, fps, (640, 480))

        frame_count = 0

        while cap.isOpened():

            ret, frame = cap.read()

            if ret:
                out.write(frame)
                frame_count = frame_count + 1

            if frame_count > self.frame_nbr:
                break

        cap.release()
        out.release()


    def extract_frames(self, video_path):
        """Extract frames from a video.

        Args:
            video_path: A string representing the path to the video.

        Returns:
            A set of jpg images corresponding to the various frames in the source
            video.
        """

        video = cv2.VideoCapture(video_path)

        frame_count = 0

        ret, frame = video.read()

        while ret:

            cv2.imwrite(os.path.join(self.path, "frame%d.jpg" % frame_count), frame)

            ret, frame = video.read()

            logging.info("read frame %s", str(frame_count))

            frame_count += 1
