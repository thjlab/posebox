"""
Main handler for the PoseBox server component.
"""

import os
import logging
import sys

import utils
import camera as cam
import socks
import segmentation as sgm


def start_server():
    """Run the posebox main loop."""

    config_file = utils.argsparser(sys.argv)
    environ = utils.init_environ(config_file)

    utils.init_logger(environ['general'])
    logger = logging.getLogger('__main__')
    logger.info("PoseBox server - hello")

    utils.create_dir(environ['capture']['inbox_folder'])

    logger.info("initializing OpenCV Caffe model")
    segnet = sgm.Segmentation(environ['opencv'])

    logger.info("initializing Camera instance")
    camera = cam.Camera(path=environ['capture']['inbox_folder'])

    logger.info("initializing server socket")
    server_socket = socks.init_server_socket()

    while True:
        server_socket.listen(5)
        logger.info("waiting for incoming connections")
        client, addr = server_socket.accept()
        logger.info("incoming connection from %s", str(addr))

        logger.info("receiving video")

        video_size = int(socks.receive_bytes_to_string(client))
        socks.send_msg(client, 'OK FRAME')
        socks.receive_item(client, video_size,
                           environ['capture']['inbox_folder'],
                           environ['capture']['video_name'])
        logger.info("video received")

        logger.info("extracting frames")
        camera.extract_frames(os.path.join(environ['capture']['inbox_folder'],
                                           environ['capture']['video_name']))

        logger.info("starting image segmentation")

        pts_list = []

        for frame_nbr in range(int(environ['capture']['frame_nbr'])):

            image = os.path.join(environ['capture']['inbox_folder'],
                                 "frame%d.jpg" % frame_nbr)

            frame = sgm.get_image_frames(image)

            pts = segnet.get_keypoints_coordinates(segnet.get_cvnet_output(frame),
                                                   frame)

            logger.info("segmentation of frame %s completed", str(frame_nbr))

            pts_list.append(pts)

        logger.info("segmentation coefficients: %s", str(pts_list))

        logger.info("sending frame received ack")
        socks.send_msg(client, 'OK FRAME')

        logger.info("frame processing completed")

        logger.info("sending coefficients")
        socks.waiting_for_ack(client, "COEFF")
        socks.send_msg(client, str(pts_list))

        logger.info("coefficients sent")

        utils.purge_files(os.path.join(environ['capture']['inbox_folder'],
                                       "frame*.jpg"))
        utils.purge_files(os.path.join(environ['capture']['inbox_folder'],
                                       environ['capture']['video_name']))

        logger.info("closing sockets")
        client.close()


if __name__ == '__main__':
    start_server()
