"""
Main runfile for the PoseBox client-side application.
"""

import os
import logging
import time
import sys

from ast import literal_eval

import utils
import camera
import net
import socks


def launch_cloud_instance(cloud, instance):
    """Start a cloud instance.

    Args:
        cloud: A string defining the cloud to connect to.
        instance: A dict with instance details.
        net: A dict with net details.
        volume: A dict with volume details.

    Returns:
        A tuple representing the created Cloud instance and a bool value, 'active':
            True: the server was already active.
            False: the server was not active.
    """

    target_cloud = net.Cloud(cloud, instance)

    if target_cloud.is_instance_created():

        if target_cloud.get_instance_status() != 'ACTIVE':

            target_cloud.start_instance()
            return (target_cloud, True)

        return (target_cloud, True)

    target_cloud.create_instance()

    return (target_cloud, False)


def run_cloud_command(remote_ip, username, keyfile, command, retry=10):
    """Executes an arbitraty command on the cloud instance and retrieve
    output.

    Args:
        remote_ip: A string representing the remote IP to connect to.
        username: A string representing the username to employ.
        keyfile: A string representing the keyfile path.
        retry: An int representing the number of ssh connection attemps to do.
        command: A string representing the remote command to execute.

    Returns:
        A tuple including std streams (stdin, stdout, stderr).
    """

    connection = net.Ssh(remote_ip, username, keyfile)

    connection.remote_connect(retry)

    return (connection, connection.exec_command(command))


def instance_service_command(was_active, posebox_folder, opencv_folder):
    """Determine which command to execute on the instance based on its previous
    power status.

    Args:
        was_active = A bool indicating if the server was already in an active
        state.
        posebox_folder = A string representing the abs path of the posebox folder.
        opencv_folder = A string representing the abs path of the opencv folder.

    Returns:
        A string representing the command to execute on the instace.
    """

    if was_active:
        return "sudo systemctl start posebox.service"

    return ("echo 'nameserver 8.8.8.8' |" +
            " sudo tee /etc/resolv.conf > /dev/null" +
            " ; cd " + posebox_folder.rstrip('posebox/') +
            " ; sudo git clone https://gitlab.com/thjlab/posebox.git" +
            " ; cd posebox ; sudo git checkout -q origin/dev" +
            " ; sudo mv " + os.path.split(opencv_folder)[1] + " ./" +
            " ; sudo mv posebox.serv* $(sudo pkg-config systemd --variable=systemdsystemunitdir)" +
            " ; sudo systemctl start posebox.service")


def run_posebox():
    """Runs the posebox main loop."""

    config_file = utils.argsparser(sys.argv)
    environ = utils.init_environ(config_file)

    utils.init_logger(environ['general'])
    logger = logging.getLogger('run_posebox')

    utils.create_dir(environ['capture']['capture_folder'])

    _cloud, was_active = launch_cloud_instance(environ['cloud']['name'],
                                               environ['instance'])

    command = instance_service_command(was_active,
                                       environ['general']['server_folder'],
                                       environ['opencv']['folder'])

    connection, output = run_cloud_command(environ['instance']['ip'],
                                           environ['ssh']['username'],
                                           environ['ssh']['keyfile'],
                                           command)

    output[1].readlines()

    connection.client.close()

    logger.info("initializing camera")

    cam = camera.Camera(path=environ['capture']['capture_folder'],
                        output=environ['capture']['video_name'],
                        frame_nbr=environ['capture']['frame_nbr'])
    time.sleep(5)

    logger.info("connecting to instance %s", environ['instance']['ip'])

    for count in range(1):

        client_socket = socks.init_client_socket(str(environ['instance']['ip']))

        logger.info("iteration %s, capturing video", str(count))
        cam.write_video(fps=environ['capture']['fps'])
        logger.info("video captured")

        logger.info("sending video")
        video_loc = os.path.join(environ['capture']['capture_folder'] +
                                 environ['capture']['video_name'])
        socks.send_item_size(client_socket, video_loc)

        socks.waiting_for_ack(client_socket)

        socks.send_item(client_socket, video_loc)

        logger.info("waiting for video reception")
        socks.waiting_for_ack(client_socket)

        logger.info("video sent")

        logger.info("receiving coefficients")

        socks.send_msg(client_socket, "OK COEFF")
        recv_string = socks.receive_bytes_to_string(client_socket)
        utils.purge_files(video_loc)

        logger.info("coefficients received")

        if recv_string != "[]":

            recv_coeff = literal_eval(recv_string)
            logger.info("coeffs received: %s", str(recv_coeff))

        else:
            logger.warning("no coefficients for this frame")

    logger.info("closing client")


if __name__ == '__main__':
    run_posebox()
