#pylint: disable=import-error,no-member
"""
Module supporting the Segmentation class, responsible for generating point
coordinates representing the various parts of a human body.

function get_image_frames: get a numpy array out of an image path.

function draw_keypoints: draw and save keypoints on an image in numpy array.

function initialize_cvnet: returns a opencv dnn network object after Caffe proto
and weight files.

class Segmentation: contains the builder and the main segmentation functions.

"""


import cv2


def get_image_frames(image_path):
    """Get a numpy array out of an image path.

    Args:
        image_path: A string representing the path to an image file.

    Returns:
        A numpy array of the image pointed by the image_path
    """

    frame = cv2.imread(image_path)

    return frame


def draw_keypoints(frame, points, savepath="frame_keypoints.jpg"):
    """Draw and save keypoints on an image in numpy format.

    Args:
        frame = A numpy array representing the image to draw keypoints on.
        points = A list representing points coordinates.
        savepath = A string representing the path to save the image to.

    Returns:
        None
    """

    index = 0

    for point in points:

        if point:

            cv2.circle(frame, (int(point[0]), int(point[1])), 8, (0, 255, 255),
                       thickness=-1, lineType=cv2.FILLED)

            cv2.putText(frame, "{}".format(index), (int(point[0]), int(point[1])),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2,
                        lineType=cv2.LINE_AA)

        index += 1

    cv2.imwrite(savepath, frame)


def initialize_cvnet(proto, weights):
    """Initialize a cv2 dnn net object based on Caffe proto and weights file.

    Args:
        proto: A string representing the path to a Caffe protofile.
        weights: A string representing the path to a Caffe weight file.

    Returns:
        A OpenCV DNN network object.
    """

    return cv2.dnn.readNetFromCaffe(proto, weights)


class Segmentation:
    """Class responsible for generating point coordinates for different parts
    of the human body.

    Relies on Caffe model (proto) and weights files, and invokes OpenCV DNN
    backend functions for generating the network.

    Attributes:
        cvnet: A OpenCV DNN network object.
        npoints: An int representing the number of output segpoints to keep.
        threshold: A float representing the threshold value for the model detection.
        pose_pairs: A list including integer lists representing the different
        segments and point coordinates in the output.
    """


    def __init__(self, cvnet_meta):
        """Default Segmentation builder."""

        self._cvnet = initialize_cvnet(cvnet_meta['model'], cvnet_meta['weights'])
        self._npoints = cvnet_meta['npoints']
        self._threshold = cvnet_meta['threshold']
        self._pose_pairs = cvnet_meta['pose_pairs']


    @property
    def cvnet(self):
        """Getter for opencv network instance cvnet.

        Args:
            None

        Returns:
            An opencv network instance.
        """

        return self._cvnet


    @cvnet.setter
    def cvnet(self, cvnet_meta):
        """Setter for the opencv network instance cvnet.

        Args:
            cvnet_meta = A dictionary containing mandatory parameters for the
            cvnet builder (proto and weights filepaths).

        Returns:
            None
        """

        self._cvnet = initialize_cvnet(cvnet_meta['model'], cvnet_meta['weights'])


    @property
    def npoints(self):
        """Getter for the npoints int attribute.

        Args:
            None

        Returns:
            An int representing the number of points to consider in the segmentation
            output.
        """

        return self._npoints


    @npoints.setter
    def npoints(self, cvnet_meta):
        """Setter for the npoints attribute.

        Args:
            cvnet_meta = A dictionary containing mandatory parameter for the
            setter (npoints key).

        Returns:
            None
        """

        self._cvnet = cvnet_meta['npoints']


    @property
    def threshold(self):
        """Getter for the threshold float attribute.

        Args:
            None

        Returns:
            A float representing the threshold value for the segmentation output
            keypoints detection.
        """

        return self._threshold


    @threshold.setter
    def threshold(self, cvnet_meta):
        """Setter for the threshold attribute.

        Args:
            cvnet_meta = A dictionary containing mandatory parameter for the
            setter (threshold key).

        Returns:
            None
        """

        self._threshold = cvnet_meta['threshold']


    @property
    def pose_pairs(self):
        """Getter for the pose_pairs list attribute.

        Args:
            None

        Returns:
            A list representing the pose_pairs values needed when constructing
            segments between keypoints.
        """

        return self._pose_pairs


    @pose_pairs.setter
    def pose_pairs(self, cvnet_meta):
        """Setter for the pose_pairs attribute.

        Args:
            cvnet_meta = A dictionary containing mandatory parameter for the
            setter (pose_pairs key).

        Returns:
            None
        """

        self._pose_pairs = cvnet_meta['pose_pairs']


    def get_cvnet_output(self, frame, in_width=368, in_height=368):
        """Get the output of the segmentation process done on arg frame with
        self.cvnet.

        Args:
            frame = A numpy array representing a frame to perform the segmentation
            on.
            in_width = An int representing the input image width for the network.
            in_height = An int representing the input image height for the network.

        Returns:
            output = A 4D matrix with:
                []: image id.
                [][]: keypoint index.
                [][][]: output map height.
                [][][][]: output map width.
        """

        # Image blob, model network ingress.
        img_blob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (in_width, in_height),
                                         (0, 0, 0), swapRB=False, crop=False)
        self._cvnet.setInput(img_blob)

        output = self._cvnet.forward()

        return output


    def get_keypoints_coordinates(self, output, frame):
        """Get the keypoint coordinates based on the cvnet output for frame.

        Args:
            output = A 4D matrix representing the cvnet output for frame.
            frame = A numpy array representing the frame processed by the network.

        Returns:
            A list of keypoints coordinates.
        """

        points = []

        for i in range(self._npoints):

            # confidence map of corresponding body's part.
            prob_map = output[0, i, :, :]

            # Find global maxima of the prob_map.
            _minval, prob, _minloc, point = cv2.minMaxLoc(prob_map)

            # Scale the point to fit on the original image
            x_width = (frame.shape[1] * point[0]) / output.shape[3] # width
            y_height = (frame.shape[0] * point[1]) / output.shape[2] # height

            if prob > self._threshold:

                points.append((int(x_width), int(y_height)))

            else:

                points.append(None)

        return points


    def get_segment_coefficients(self, points):
        """Get the slopes of the segments for each point pairs included in
        self._pose_pairs.

        Args:
            points = A list of tuple representing the coordinates of each keypoints.

        Returns:
            A list of tuple including the point pairs and their segment slope.
        """

        coeffs = []

        for pair in self._pose_pairs:

            pair_a = points[pair[0]]
            pair_b = points[pair[1]]

            if pair_a and pair_b:

                try:
                    slope = float((pair_b[1] - pair_a[1]) / (pair_b[0] - pair_a[0]))
                    coeffs.append(slope)
                except ZeroDivisionError:
                    coeffs.append(pair.append(None))

        return coeffs


    def draw_skeleton(self, frame, points, savepath="frame_skeleton.jpg"):
        """Draw and save skeleton on an image in numpy format.

        Args:
            frame = A numpy array representing the image to draw keypoints on.
            points = A list representing points coordinates.
            savepath = A string representing the path to save the image to.

        Returns:
            None
        """

        for pair in self._pose_pairs:

            pair_a = points[pair[0]]
            pair_b = points[pair[1]]

            if pair_a and pair_b:

                cv2.line(frame, pair_a, pair_b, (0, 255, 255), 2)
                cv2.circle(frame, pair_a, 8, (0, 0, 255), thickness=-1,
                           lineType=cv2.FILLED)

        cv2.imwrite(savepath, frame)
