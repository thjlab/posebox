"""
Module supporting various helpers and utility functions.

function argsparser: Command line args parser function.

function join: Custom YAML tag used to concatenate two YAML fields.

function init_environ: Return the configuration parameters from a YAML file.

function init_logger: Initialize the logger function.

function create_dir: Create the directory dirname if it does not exist.

"""

import os
import glob
import argparse
import logging
import yaml #pylint: disable=import-error


def argsparser(_arguments):
    """Command line args parser used to retrieve path to the configuration file.

    Args:
        arguments = A list of string representing the arguments to pass to the
        function

    Returns:
        A string representing the path to the configuration file.
    """

    parser = argparse.ArgumentParser(description="PoseBox Parser")
    parser.add_argument('-c', '--config', help='Path to the configuration file',
                        nargs='?', default='config.yaml', type=str)

    args = vars(parser.parse_args())
    config = args['config']

    return config


def join(loader, node):
    """Custom YAML tag used to concatenate two YAML fields together.

    More info on YAML custom tags:
    https://stackoverflow.com/questions/2063616/
    how-to-reference-a-yaml-setting-from-elsewhere-in-the-same-yaml-file

    More info on feeding os.path.join with a list of args:
    https://docs.python.org/2/tutorial/controlflow.html#unpacking-argument-lists

    Args:
        loader = A YAML loader.
        node = A YAML node.

    Returns:
        A string representing the concatenated fields.
    """

    seq = loader.construct_sequence(node)
    return os.path.join(*[str(i) for i in seq])


def init_environ(configfile='config.yaml'):
    """Read and return the configuration parameters from a YAML file.

    Args:
        configfile = A string representing the path to the YAML configuration
        file.

    Returns:
        A dict containing all keys/values pertaining to the application.
    """

    yaml.add_constructor('!join', join)

    with open(configfile, 'r') as stream:

        try:
            environ = yaml.load(stream)
            return environ

        except yaml.YAMLError as err:
            print("Cannot load configuration file: %s", str(err))


def init_logger(log_environ):
    """Initialize the logger function for the project.

    Args:
        debug: A bool defining debug mode (verbose output) or not.

    Returns:
        None
    """

    logfile = log_environ['logfile']
    logging.basicConfig(filename=logfile,
                        filemode="w",
                        level=logging.DEBUG,
                        format='%(asctime)s %(name)s %(levelname)s: %(message)s',
                        datefmt='%b %d %H:%M:%S')

    console = logging.StreamHandler()

    if log_environ['debug_on']:
        console.setLevel(logging.DEBUG)
    else:
        console.setLevel(logging.ERROR)

    logging.getLogger("").addHandler(console)


def create_dir(dirname):
    """Create the directory dirname if it does not exist.

    Args:
        dirname = A string representing the path of the directory to create.

    Returns:
        None
    """

    if not os.path.exists(dirname):
        os.makedirs(dirname)


def purge_files(filepath):
    """Delete file passed in argument. Accepts wildcards.

    Args:
        filename = A string representing the file of the file to delete.

    Returns:
        None
    """

    for filename in glob.glob(filepath):
        os.remove(filename)
